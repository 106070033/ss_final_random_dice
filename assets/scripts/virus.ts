// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class virus extends cc.Component {

    @property(cc.Label)
    label: cc.Label = null;

    @property
    text: string = 'hello';

    private aDown:boolean = false;

    isPause:boolean = false;

    virus_speed:number = 0;

    virus_x:number = 0;

    virus_y:number = 0;

    virus_dir:number = 0;

    virus_life:number = 10;
    /**
     * dir == 0:up
     * dir == 1:right
     * dir == 2:down
     * dir == 3:left
     */

    isDead:boolean = false;
    // LIFE-CYCLE CALLBACKS:

    onLoad () {
        cc.director.getPhysicsManager().enabled = true;
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_DOWN,this.onKeyDown,this);
        cc.systemEvent.on(cc.SystemEvent.EventType.KEY_UP,this.onKeyup,this);
        this.virus_speed = 50;
    }

    start () {

    }
    onKeyDown(event){
        cc.log("Keydown :" + event.keyCode);
        if(event.keyCode == cc.macro.KEY.a){
            this.aDown = true;
            this.isPause = !this.isPause;
        }
    }
    onKeyup(event){
        if(event.keyCode == cc.macro.KEY.a){
            this.aDown = false;
        }
    }
    
    playmovement(dt){
        if(this.isDead){
            this.destroy();
        }
        else{
            //cc.log("dir",this.virus_dir);
            //cc.log("position:",this.node.x,this.node.y);
            if(this.virus_dir == 0){//up
                this.virus_y = this.node.y +this.virus_speed * dt;
                this.node.y = this.virus_y;
            }
            else if(this.virus_dir == 1){//right
                this.virus_x = this.node.x +this.virus_speed * dt;
                this.node.x = this.virus_x;
            }
            else if(this.virus_dir == 2){//down
                this.virus_y = this.node.y -this.virus_speed * dt;
                this.node.y = this.virus_y;
            }
            else if(this.virus_dir == 3){//left
                this.virus_x = this.node.x -this.virus_speed * dt;
                this.node.x = this.virus_x;
            }

        }
    }
    update (dt) {
        if(!this.isPause){
            this.playmovement(dt);
        }
    }
    onBeginContact(contact , self ,other){
        if(other.node.group == "track"){
            this.virus_speed = 50;
            if(other.node.name == "brown_up"){
                this.virus_dir = 0;
            }
            else if(other.node.name == "brown_right"){
                this.virus_dir = 1;
            }
            else if(other.node.name == "brown_down"){
                this.virus_dir = 2;
            }
            else if(other.node.name == "brown_left"){
                this.virus_dir = 3;
            }
        }
        else if(other.node.group == "bullet"){
            this.virus_life -- ;

        }
        else {
            this.virus_speed = 0;
        }
    }

}
