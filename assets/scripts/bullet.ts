// Learn TypeScript:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/typescript.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/typescript.html
// Learn Attribute:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/reference/attributes.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/reference/attributes.html
// Learn life-cycle callbacks:
//  - [Chinese] https://docs.cocos.com/creator/manual/zh/scripting/life-cycle-callbacks.html
//  - [English] http://www.cocos2d-x.org/docs/creator/manual/en/scripting/life-cycle-callbacks.html

const {ccclass, property} = cc._decorator;

@ccclass
export default class NewClass extends cc.Component {

    target_pos : cc.Vec2 = new cc.Vec2(0,0);

    bullet_speed : number = 50;
    // LIFE-CYCLE CALLBACKS:

    // onLoad () {}
    public init(node:cc.Node){
        this.set_bullet_init_pos(node);
        this.bullet_move();
    }
    start () {

    }
    get_target_pos(){/////不知道怎麼寫

    }
    set_bullet_init_pos(node:cc.Node){
        this.node.position = node.position;
    }
    bullet_move(){
        //get target position
        this.get_target_pos();
        var target_point = this.target_pos;
        var point =new  cc.Vec2(this.node.x,this.node.y);
        var delta = target_point.sub(point);
        var distance = delta.mag();
        var new_x = point.x + this.bullet_speed * delta.x /distance;
        var new_y = point.y + this.bullet_speed * delta.y /distance;
        this.node.x = new_x;
        this.node.y = new_y;
    }
    // update (dt) {}
}
